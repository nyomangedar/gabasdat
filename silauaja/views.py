from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm
from .forms import CreateUserForm
# Create your views here.
def loginpage(request):
    return render(request, 'login.html')

def staffRegist(request):
    return render(request, 'stafform.html')

def customerRegist(request):
    return render(request, 'customerform.html')

def courierRegist(request):
    return render(request, 'courierform.html')


def register(request):
    form  = CreateUserForm()

    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
    context = {'form':form}
    return render(request, 'register.html', context)
