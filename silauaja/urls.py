from django.urls import path
from . import views

app_name = 'silauaja'

urlpatterns = [
    path('', views.loginpage, name='login'),
    path('staf_form', views.staffRegist, name='stafform'),
    path('courier_form', views.courierRegist, name='courierform'),
    path('customer_form', views.customerRegist, name='customerform'),
    path('register', views.register, name='regist')
]