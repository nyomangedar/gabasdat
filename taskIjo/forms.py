from django import forms
from django.db import connection, transaction, IntegrityError
from datetime import datetime

# LAUNDRY_LIST TABLE
# ======================================================================================================================================
class LaundryListForm(forms.Form):
    att = {'class' : 'form-control'}
    email = forms.ChoiceField(
        choices=[], 
        widget=forms.Select(attrs=att, choices=[])
    )
    item_amount=forms.CharField(
        max_length=20
    )
    item_code=forms.ChoiceField(
        choices=[],
        widget=forms.Select(attrs=att, choices=[])
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute('SELECT code FROM silau.item')
            lst = [ (i[0],i[0]) for i in cursor.fetchall() ]
            self.fields['item_code'].choices = lst
            self.fields['item_code'].widget.choices = lst

            cursor.execute('SELECT email FROM silau.user')
            lst2 = [ (i[0],i[0]) for i in cursor.fetchall() ]
            self.fields['email'].choices = lst2
            self.fields['email'].widget.choices = lst2


class CreateLaundryList(LaundryListForm):
    def save(self):
        cd = self.cleaned_data
        with connection.cursor() as cursor:
            cursor.execute("SELECT serial_no FROM silau.laundry_list ORDER BY serial_no DESC LIMIT 1")
            new_serial_no = int(cursor.fetchone()[0]) + 1
            now = datetime.now()
            new_timestamp = now.strftime("%Y-%m-%d %H:%M:%S")
            cursor.execute("INSERT INTO silau.laundry_list values (%s,%s,%s,%s,%s)",
                            [cd.get("email"), new_timestamp, 
                            new_serial_no, cd.get("item_amount"),
                            cd.get("item_code")])


class UpdateLaundryListForm(forms.Form):
    att = {'class' : 'form-control', 'readonly' : True}
    email = forms.CharField(
        widget=forms.TextInput(attrs=att)
    )
    transaction_date = forms.CharField(
        widget=forms.TextInput(attrs=att)
    )
    serial_no = forms.CharField(
        widget=forms.TextInput(attrs=att)
    )
    item_amount=forms.CharField(
        max_length=20
    )
    item_code=forms.ChoiceField(
        choices=[],
        widget=forms.Select(attrs={'class':'form-control'}, choices=[])
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute('SELECT code FROM silau.item')
            lst = [ (i[0],i[0]) for i in cursor.fetchall() ]
            self.fields['item_code'].choices = lst
            self.fields['item_code'].widget.choices = lst

    def save(self):
        cd = self.cleaned_data
        with connection.cursor() as cursor:
            cursor.execute("""
            UPDATE silau.laundry_list SET
            item_amount = %s, item_code = %s
            WHERE email=%s AND serial_no=%s AND transaction_date=%s;
                           """,
                           [cd.get("item_amount"), cd.get("item_code"),
                            cd.get("email"), cd.get("serial_no"),
                            cd.get("transaction_date")
                            ])
# ITEM TABLE
# ==================================================================================================================================

class ItemForm(forms.Form):
    code = forms.CharField(
        max_length=20
    )
    name = forms.CharField(
        max_length=20
    )

class CreateItem(ItemForm):
    def save(self):
        cd = self.cleaned_data
        with connection.cursor() as cursor:
            cursor.execute("INSERT INTO silau.item values (%s,%s)",
                            [cd.get("code"), cd.get("name")])

