from django.shortcuts import render,redirect
from django.db import connection, transaction, IntegrityError
from .forms import *
# Create your views here.

def laundrylist(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM silau.laundry_list")
        columns = [col[0] for col in cursor.description]
        data = [dict(zip(columns, row)) for row in cursor.fetchall()]
    context = {'data': data}
    return render(request, "laundrylist.html",context)

def createLaundryList(request):
    if request.method == 'POST':
        form = CreateLaundryList(request.POST)
        if form.is_valid():
            form.save()
            return redirect("taskIjo/laundry_list/")
    if request.method == 'GET' :
        form = LaundryListForm()
    context = {'form':form}
    return render(request, "laundryForm.html",context)

def updateLaundryList(request):
    if request.method == 'POST':
        form = UpdateLaundryListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("taskIjo/laundry_list/")
    elif request.method == 'GET':
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM silau.laundry_list where serial_no=%s LIMIT 1;",[request.GET.get('serial_no')])
            data = cursor.fetchone()
            columns = [col[0] for col in cursor.description]
            data_list = dict(zip(columns, data))
        form = UpdateLaundryListForm(initial=data_list)
    
    context = {'form' : form}
    return render(request, "updateLaundryList.html", context)

def deleteLaundryList(request):
    if request.method == 'GET':
        serial = request.GET.get('serial_no')
        try:
            with connection.cursor() as cursor:
                cursor.execute("DELETE FROM silau.laundry_list where serial_no=%s",[serial])
        except IntegrityError:
            pass
        return redirect('laundry_list/')
        

def item(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM silau.item")
        columns = [col[0] for col in cursor.description]
        data = [dict(zip(columns, row)) for row in cursor.fetchall()]
    context = {'data':data}
    return render(request, "item.html",context)

def createItem(request):
    if request.method == 'POST':
        form = CreateItem(request.POST)
        if form.is_valid():
            form.save()
            return redirect("taskIjo/laundry_list/")
    if request.method == 'GET':
        form = CreateItem()
    context = {'form':form}
    return render(request, "createItem.html",context)