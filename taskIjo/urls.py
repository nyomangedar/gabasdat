from django.urls import path
from . import views

app_name = 'taskIjo'

urlpatterns = [
    path('laundry_list/',views.laundrylist, name ='laundrylist'),
    path('create_laundry_list/',views.createLaundryList, name='createlaundrylist'),
    path('update_laundry_list/',views.updateLaundryList, name='updatelaundrylist'),
    path('item/', views.item, name='item'),
    path('create_item/', views.createItem, name='createitem'),
    path('delete_laundry', views.deleteLaundryList, name='deletelaundrylist'),
    # path('update_deliver_cost', views.updateDeliveryCost, name='updateDeliveryCost'),
    # path('create_deliver_cost', views.updateDeliveryCost, name='createeDeliveryCost'),
]