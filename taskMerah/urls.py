from django.urls import path
from . import views

urlpatterns = [
    path('service/',views.service, name ='service'),
    path('testimony/',views.testimony, name='testimony'),
    path('testimony_form/',views.testimony_form, name='testimonyform'),
    path('update_service_form/', views.update_service_form, name='updateserviceform'),
    path('update_testimony_form/', views.update_testimony_form, name='updatetestimonyform'),
]