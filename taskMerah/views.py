from django.shortcuts import render

# Create your views here.
def service(request):
    return render(request, "service.html")

def testimony(request):
    return render(request, "testimony.html")

def testimony_form(request):
    return render(request, "testimony_form.html")

def update_service_form(request):
    return render(request, "update_service_form.html")

def update_testimony_form(request):
    return render(request, "update_testimony_form.html")