from django.shortcuts import render

# Create your views here.

def updateDeliveryCost(request):
    return render(request, 'deliveryCost.html')

def create_delivery(request):
    return render(request, 'deliveryCostForm.html')