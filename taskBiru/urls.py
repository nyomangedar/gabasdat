from django.urls import path
from . import views

urlpatterns = [
    path('status/',views.status, name ='laundrylist'),
    path('create_topup_dpay/',views.create_topup_dpay, name='createlaundrylist'),
    path('update_topup_dpay/',views.update_topup_dpay, name='updatelaundrylist'),
    path('item/', views.topup_dpay, name='item'),
]