Links that work

    1. Green Task:
    https://silau-aja.herokuapp.com/taskijo/laundry_list/
    https://silau-aja.herokuapp.com/taskijo/create_laundry_list/
    https://silau-aja.herokuapp.com/taskijo/update_laundry_list/ : <-if you press the update button in laundry list
    https://silau-aja.herokuapp.com/taskijo/item/
    https://silau-aja.herokuapp.com/taskijo/create_item/


    Hardcode:
    2. Blue Task:
    https://silau-aja.herokuapp.com/taskbiru/status/
    https://silau-aja.herokuapp.com/taskbiru/create_topup_dpay/
    https://silau-aja.herokuapp.com/taskbiru/taskbiru/update_topup_dpay/
    https://silau-aja.herokuapp.com/taskbiru/taskbiru/item/

    3. Yellow Task:
    https://silau-aja.herokuapp.com/taskbiru/updateDeliveryCost/
    https://silau-aja.herokuapp.com/taskbiru/create_topup_dpay/

    4. Red Task:
    https://silau-aja.herokuapp.com/taskmerah/service/
    https://silau-aja.herokuapp.com/taskmerah/testimony/
    https://silau-aja.herokuapp.com/taskmerah/testimony_form/
    https://silau-aja.herokuapp.com/taskmerah/update_service_form/
    https://silau-aja.herokuapp.com/taskmerah/update_testimony_form/
